variables:
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  LOCAL_CAS: 'false'

  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_IMAGE_ID: '733645a5f00b437fc266ad152d8cc0d18b29cb0d'
  BST: bst --on-error continue -o arch ${SDKARCH}

  DOCKER_ELEMENTS: >-
    builder-bst2-docker.bst
    buildbox-casd-docker.bst
    buildbox-worker-docker.bst
    buildgrid-docker.bst
    lorry-docker.bst
    marge-bot-docker.bst
    debuginfod-docker.bst
  DOCKER_IMAGES: >-
    bst2
    buildbox-casd
    buildbox-worker
    buildgrid
    lorry
    marge-bot
    debuginfod

  BUILD_TASKS: 4
  BUILD_CPU_MULTIPLIER: 1

default:
  image: "${DOCKER_REGISTRY}/bst2:${DOCKER_IMAGE_ID}"

workflow:
  rules:
  - if: '$CI_MERGE_REQUEST_IID'
  - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == "true"'
  - if: '$CI_PIPELINE_SOURCE == "web"'

stages:
  - "bootstrap"
  - "build"
  - "manifest"

before_script:
  - ulimit -n 1048576

  - |
    if [ "${CI_COMMIT_REF_NAME}" = master ]; then
      export REF_TAG="${CI_COMMIT_SHA}"
    else
      export REF_TAG="dev-${CI_COMMIT_REF_SLUG}"
    fi

  # Private SSL keys/certs for pushing to the CAS server
  - |
    # Force higher parallelity
    export MAX_JOBS=$(python -c "print(round(($(nproc) * ${BUILD_CPU_MULTIPLIER}) / ${BUILD_TASKS}))")
    [ -d ~/.config ] || mkdir -p ~/.config
    [ -f /cache-certificate/server.crt ] && export LOCAL_CAS=true
    python3 -mmako.cmd templates/buildstream.conf --output-file ~/.config/buildstream.conf
    cat ~/.config/buildstream.conf

.build_and_publish:
  stage: "build"
  needs: []
  script:
    - |
      ${BST} build ${DOCKER_ELEMENTS}

    - |
      for element in ${DOCKER_ELEMENTS}; do
          ${BST} artifact checkout $element --tar - | podman load --log-level warn
      done

    - set -eu
    - podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    - |
      for IMAGENAME in ${DOCKER_IMAGES}; do
        mkdir -p digests/${IMAGENAME}
        IMAGE="${CI_REGISTRY_IMAGE}/${IMAGENAME}"
        podman push ${IMAGE} docker://${IMAGE}:${REF_TAG}-${ARCH} \
            --digestfile digests/${IMAGENAME}/${ARCH}.txt
        echo "Pushed ${IMAGE}:${REF_TAG}-${ARCH}"
      done
    - podman image list
  retry:
    max: 2
    when: runner_system_failure
  artifacts:
    when: always
    paths:
    - "${CI_PROJECT_DIR}/cache/buildstream/logs"
    - "${CI_PROJECT_DIR}/digests"
    
  interruptible: true

amd64:
  tags:
    - "x86_64"
  variables:
    ARCH: "amd64"
    SDKARCH: "x86_64"
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2
  extends: .build_and_publish

aarch64:
  variables:
    ARCH: "arm64"
    SDKARCH: "aarch64"
    BUILD_TASKS: 4
  tags:
    - "aarch64"
  extends: .build_and_publish

ppc64le:
  variables:
    ARCH: "ppc64le"
    SDKARCH: "ppc64le"
    BUILD_TASKS: 2
  tags:
    - "ppc64le"
  extends: .build_and_publish

riscv64:
  variables:
    ARCH: "riscv64"
    SDKARCH: "riscv64"
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2
  tags:
    - "riscv"
  extends: .build_and_publish
  allow_failure: true
  when: manual

manifest:
  stage: "manifest"
  script:
    - podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    - set -eu

    - |
      for IMAGENAME in ${DOCKER_IMAGES}; do
        IMAGE="${CI_REGISTRY_IMAGE}/${IMAGENAME}"
        podman manifest create ${IMAGE}
        for DIGEST_FILE in digests/${IMAGENAME}/*; do
          DIGEST=$(<${DIGEST_FILE})
          podman manifest add ${IMAGE} docker://${IMAGE}@${DIGEST}
        done
        podman manifest push --format v2s2 ${IMAGE} docker://${IMAGE}:${REF_TAG}
        if [ "${CI_COMMIT_REF_NAME}" = master ]; then
          podman manifest push --format v2s2 ${IMAGE} docker://${IMAGE}:latest
        fi
      done
